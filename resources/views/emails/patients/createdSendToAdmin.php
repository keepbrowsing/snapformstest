<div>
    hi Admin,<br/>
    New patient has been registered. <br/>
    Details are as follows:<br/><br/>

    Name: {{ $patient->full_name }},
    Email: {{ $patient->email }},
    Gender: {{ $patient->gender }},
    DOB: {{ $patient->dob }},
    Address: {{ $patient->address }},

</div>
