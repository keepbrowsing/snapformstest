@extends('master')

@section('title', 'Page Title')

@section('content')
    <h1>Register New Patient</h1>

    @if($errors->any())
        {!! implode('', $errors->all('<div class="alert alert-danger">:message</div>')) !!}
    @endif

    @if (session('success'))
        <div class="alert alert-success">
            Thank you, Patient record has been saved successfully.
        </div>
    @endif

    <form method="POST" action="/patient/register">
        @csrf

        <div class="form-group">
            <label for="exampleInputEmail1">Full Name</label>
            <input required type="text" class="form-control" name="full_name" id="text-fullname" placeholder="Enter full name">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input required type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">
            <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
        </div>

        <div class="form-group">
            <label for="text-dob">Date of Birth</label>
            <input type="text" required name="dob" class="form-control" id="text-dob" aria-describedby="dobHelp">
            <small id="dobHelp" class="form-text text-muted">enter in format yy-mm-dd eg. 2020-05-14</small>
        </div>
        <div class="form-group">
            <label for="text-address">Address</label>
            <input type="text" name="address" required class="form-control" id="text-address" placeholder="Enter full address">
        </div>

        <div class="form-group">
            <label>Gender</label>
            <select name="gender" required class="form-control">
                <option value="Male">Male</option>
                <option value="Female">Female</option>
                <option value="NA">NA</option>
            </select>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
