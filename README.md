# XYZ hostipal System

This web based software is a demo project. The main purpose of this project is to save patient's records into the system.

## Installation
1. Clone the repo from GIT `https://bitbucket.org/keepbrowsing/snapformstest/src/master/`
2. Composer install
3. Copy the env.example file and create .env file
4. Set db credentials and other required keys in .env file
5. Create a mysql database named 'hostipal'
6. Php artisan migrate

## Usage
1. Register new patient for the hospital
2. Send registration email to the new patient and to the admin.

### To run the Tests:
```
php artisan config:clear
vendor/bin/phpunit
```

## Contributing

1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## Credits

TODO: Write credits

## License

TODO: Write license
