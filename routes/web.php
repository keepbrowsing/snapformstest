<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PatientController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PatientController::class, 'register']);

Route::prefix('patient')->group(function () {
    Route::get('/register', [PatientController::class, 'register'])->name('patient.register.show');
    Route::post('/register', [PatientController::class, 'store'])->name('patient.register.save');
});
