<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\JsonResponse;
use Tests\TestCase;

class PatientControllerTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_registration_form_loads()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText('Register New Patient');
    }

    public function test_registration_validation_works()
    {
        $postValues = [
          'email' => 'notvalid',
          '_token' => csrf_token(),
          'full_name' => 'isok',
          'dob' => '444-44', //invalid date
          'gender' => 'Male',
          'address' => '',
        ];

        $response = $this->post(route('patient.register.save'), $postValues);

        $response->assertStatus(302)
        ->assertSessionHasErrors(['email', 'dob', 'address']);

        $this->assertDatabaseMissing('patients', [
            'full_name' => 'isok',
        ]);
    }

    // test_valid_data_saves_in_db
    // test_email_is_sent_to_admin
    // test_email_is_sent_to_new_patient
}
