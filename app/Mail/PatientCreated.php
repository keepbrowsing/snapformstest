<?php

namespace App\Mail;

use App\Models\Patient;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class PatientCreated extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The Patient instance.
     *
     * @var \App\Models\Patient
     */
    public $patient;

    /**
     * Create a new message instance.
     *
     * @param  \App\Models\Patient  $patient
     * @return void
     */
    public function __construct(Patient $patient)
    {
        $this->patient = $patient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.patients.created');
    }
}
