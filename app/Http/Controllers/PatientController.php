<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePatientRequest;
use App\Mail\PatientCreated;
use App\Models\Patient;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class PatientController extends Controller
{
    public function register()
    {
        return view('patients.register');
    }

    public function store(StorePatientRequest $request)
    {
        $data = $request->all();

        $patient = Patient::create([
            'full_name' => $data['full_name'],
            'email' => $data['email'],
            'address' => $data['address'],
            'dob' => $data['dob'],
            'gender' => $data['gender'],
        ]);

        if ($patient) {
            // note: mail must be in queue because its a busy webpage,
            // we cant afford to wait for email to be send instantly.

            // send email to the patient
            //Mail::to($patient->email)
            //    ->queue(new PatientCreated($patient));

            // send email to the admin
            //Mail::to('ron@snapforms.com.au')
            //    ->queue(new PatientCreatedSendToAdmin($patient));

            return back()->with('success', true);
        }
    }


}
